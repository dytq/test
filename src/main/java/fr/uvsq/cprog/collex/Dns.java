package fr.uvsq.cprog.collex;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;


/**
 * Classe DNS.
 *  */
public class Dns {

    private Properties properties = new Properties();

    List<DnsItem> dnsItems = new ArrayList<>();

    public Dns(final String bd_name_path) {

        try {
            FileInputStream f = new FileInputStream(bd_name_path);
            properties.load(f);
            f.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  Méthode qui à partir d'une addresse IP retourne une instance de DnsItem
     *  @param L'addresse Ip
     *  @param il faut un nom de domaine pour pouvoir faire une instance d'une entrée dns
     *  */
    public DnsItem getItem(AddressIp addressIp, String nom_domaine) {
        Iterator <Object> it = properties.keySet().iterator();
        while(it.hasNext()) {
            String key = (String) it.next();
            if(properties.get(key).toString().equals(addressIp.toString())) {
                DnsItem dnsItem = new DnsItem(addressIp,new NomMachine(key),nom_domaine);
                return dnsItem;
            }
        }
        return null;
    }

    /**
     * Méthode qui a partir d'un nom de machine retourne une instance de Dnsitem
     * @param Le nom de la machine
     * @param il faut un nom de domaine pour pouvoir faire une instance d'une entrée dns
     * */
    public DnsItem getItem(NomMachine nomMachine, String nom_domaine) {
        DnsItem dnsItem = new DnsItem(new AddressIp((String) properties.get(nomMachine.toString())),nomMachine,nom_domaine);
        return dnsItem;
    }

    /**
     *  Méthode qui retourne une liste d'item à partir du nom de domaine.
     *  */
    public ArrayList<DnsItem> getItems(String nom_domaine) {
        return null;
    }
}
