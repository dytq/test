package fr.uvsq.cprog.collex;

import org.graalvm.compiler.nodes.memory.address.AddressNode.Address;

/**
 * Classe pour la saisi d'une entrée DNS.
 *
 */
public class DnsItem {

    private AddressIp addressIp;
    private NomMachine nomMachine;
    private String nomDomaine;

    public DnsItem(AddressIp addressIp, NomMachine nomMachine, String nomDomaine) {
        this.addressIp = addressIp;
        this.nomMachine = nomMachine;
        this.nomDomaine = nomDomaine;
    }

    @Override
    public String toString() {
        return "DnsItem [addressIp=" + addressIp + ", nomDomaine=" + nomDomaine + ", nomMachine=" + nomMachine + "]";
    }


}
