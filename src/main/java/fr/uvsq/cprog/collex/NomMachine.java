package fr.uvsq.cprog.collex;

import java.util.Map;

public class NomMachine {

    private String machine;

    public NomMachine(String machine) {
        if((Object) machine == null) {
            throw new IllegalArgumentException();
        }
        if(machine == "") {
            throw new IllegalArgumentException();
        }
        this.machine = machine;
    }

    public String getMachine() {
        return machine;
    }

    public void setMachine(String machine) {
        this.machine = machine;
    }

    @Override
    public String toString() {
        return machine.toString();
    }
}
