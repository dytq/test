package fr.uvsq.cprog.collex;

import java.util.ArrayList;
import java.util.List;

public class AddressIp {

    List<Byte> address = new ArrayList<>();

    public AddressIp(Byte byte1, Byte byte2, Byte byte3, Byte byte4) {
        if (byte1 == null || byte2 == null || byte3 == null || byte4 == null) {
            throw new IllegalArgumentException();
        }
        address.add(byte1);
        address.add(byte2);
        address.add(byte3);
        address.add(byte4);
    }

    public AddressIp(int byte1, int byte2, int byte3, int byte4) {
        if((byte1 < 0 || byte1 > 255) || (byte2 < 0 || byte2 > 255) || (byte3 < 0 || byte3 > 255) || (byte4 < 0 || byte4 > 255)) {
            throw new IllegalArgumentException();
        }
        address.add((byte) byte1);
        address.add((byte) byte2);
        address.add((byte) byte3);
        address.add((byte) byte4);
    }

    public AddressIp(String address) {
        if((Object) address == null) {
            throw new IllegalArgumentException();
        }
        for(int i = 0; i < 4; i++) {
            String[] res = address.split("[.]"); // --!--
            /* Byte vérifie l'overflow pour des string non signé */
            Integer oct;
            oct = Integer.valueOf(res[i]);
            /* Borne redéfini entre 0 et 255 pour des nombres non signé */
            if (oct > 255 || oct < 0) {
                throw new NumberFormatException();
            }
            this.address.add(oct.byteValue());
        }
    }

    public List<Byte> getAddress() {
        return address;
    }


    public void setAddress(List<Byte> address) {
        this.address = address;
    }

    @Override
    public String toString() {

        StringBuilder s = new StringBuilder();

        for (Byte b : address) {
            s.append(b & 0xFF);
            s.append('.');
        }
        s.deleteCharAt(s.length() - 1);

        return s.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AddressIp other = (AddressIp) obj;
        if (address == null) {
            if (other.address != null)
                return false;
        } else if (!address.equals(other.address))
            return false;
        return true;
    }
}
