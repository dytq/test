package fr.uvsq.cprog.collex;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DnsTest {

    private Dns dns;
    private AddressIp addressIp;
    private NomMachine nomMachine;
    private DnsItem dnsItem;

    @Before
    public void testbefore() {
        dns = new Dns("test_hosts.txt");
        addressIp = new  AddressIp("192.168.0.0");
        nomMachine =  new NomMachine("mpl");
        dnsItem = new DnsItem(addressIp, nomMachine, "domain.local");
    }

    @Test
    public void test_get_item_from_addressip() {
        assertEquals(dnsItem.toString(), dns.getItem(addressIp,"domain.local").toString());
    }

    @Test
    public void test_get_item_from_nomMachine() {
        assertEquals(dnsItem.toString(), dns.getItem(nomMachine,"domain.local").toString());
    }
}
