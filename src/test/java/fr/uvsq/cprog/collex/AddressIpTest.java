package fr.uvsq.cprog.collex;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AddressIpTest {

    private AddressIp addr1 = new AddressIp("192.168.0.0");
    private AddressIp addr2 = new AddressIp(192,168,0,1);

    @Before
    public void testbefore() {

    }

    @Test
    public void testToString_addr1() {
        assertEquals(addr1.toString(),"192.168.0.0");
    }

    @Test(expected = NumberFormatException.class)
    public void test_string_arg_overflow(){
        AddressIp addr = new AddressIp("192.400.0.3");
    }

    @Test(expected = NumberFormatException.class)
    public void test_string_arg_neg(){
        AddressIp addr = new AddressIp("192.-34.1.3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_arg_neg() {
        AddressIp addr = new AddressIp(192, -168, 32, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_arg_overflow() {
        AddressIp addr = new AddressIp(192, 500, 0, 0);
    }

    @Test
    public void testToString_addr2() {
        assertEquals(addr2.toString(),"192.168.0.1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_null_arg1() {
        AddressIp addr = new AddressIp(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_null_arg2() {
        AddressIp addr = new AddressIp(null, null, null, null);
    }
}
