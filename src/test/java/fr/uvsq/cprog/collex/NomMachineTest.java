package fr.uvsq.cprog.collex;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class NomMachineTest {

    private NomMachine nom_machine1 = new NomMachine("test");
    private NomMachine nom_machine1prime = new NomMachine("test");

    @Before
    public void testbefore() {
    }

    @Test
    public void testToString() {
        assertEquals(nom_machine1.toString(),"test");
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_void_arg() {
        NomMachine nom = new NomMachine("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_null_arg() {
        NomMachine nom = new NomMachine(null);
    }
}
